<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'active'];

    protected $table = 'products';
    //  Eloquent Задание 1: указать что таблица - products

    /**
     * Функция для выборки трёх активных продуктов
     * @return mixed
     */
    public static function getActiveLimitProducts(){

            return self::select('*')
                ->where('active', true)
                ->orderBy('created_at', 'desc')
                ->limit('3')
                ->get();
    }

    public function scopeActive($query){
        return $query->where('active', true);
    }
}
