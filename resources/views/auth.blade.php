<!--  Blade Задание 4: Сделать проверку авторизован пользователь или нет -->
<!-- Если да то вывести ID пользователя -->
<!-- ID пользователя вывести внутри конструкции с проверкой -->
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Hello</title>
</head>
<body class="antialiased">
@if (Auth::check())
    <h2>id пользователя: </h2><p>{{Auth::id()}}</p>
@endif
</body>
</html>
