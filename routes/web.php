<?php

use Illuminate\Support\Facades\Route;

// Route Задание 1: По GET урлу /hello отобразить view - /resources/views/hello.blade (без контроллера)
Route::view('/hello', 'hello');

// Route Задание 2: По GET урлу / обратиться к IndexController, метод index
Route::get('/', '\App\Http\Controllers\IndexController@index');

// Route Задание 3: По GET урлу /page/contact отобразить view - /resources/views/pages/contact.blade
// с наименованием роута - contact
Route::view('/page/contact', '/pages/contact')->name('contact');


// Route Задание 4: По GET урлу /users/[id] обратиться к UserController, метод show
// без Route Model Binding. Только параметр id
Route::get('/users/{id}', '\App\Http\Controllers\UserController@show');


// Route Задание 5: По GET урлу /users/bind/[user] обратиться к UserController, метод showBind
// но в данном случае используем Route Model Binding. Параметр user
Route::get('/users/bind/{user}', [App\Http\Controllers\UserController::class, 'showBind']);



// Route Задание 6: Выполнить редирект с урла /bad на урл /good
Route::get('/bad', function () { return redirect('/good');});


// Route Задание 7: Добавить роут на ресурс контроллер - UserCrudController с урлом - /users_crud
Route::resource('/users_crud', App\Http\Controllers\UserCrudController::class);



// Route Задание 8: Организовать группу роутов (Route::group()) объединенных префиксом - dashboard
    // Задачи внутри группы роутов dashboard
    // Route Задание 9: Добавить роут GET /admin -> Admin/IndexController -> index
    // Route Задание 10: Добавить роут POST /admin/post -> Admin/IndexController -> post
Route::prefix('dashboard')->group(function () {
    Route::get('/admin', [\App\Http\Controllers\Admin\IndexController::class, 'index']);

    Route::get('/admin/post', [\App\Http\Controllers\Admin\IndexController::class, 'post']);
});


// Route Задание 11: Организовать группу роутов (Route::group()) объединенных префиксом - security и мидлваром auth

    // Задачи внутри группы роутов security
    // Задание 12: Добавить роут GET /admin/auth -> Admin/IndexController -> auth

Route::group([
    'prefix' => 'security',
    'middleware' => 'auth'
], function () {
    Route::get('admin/auth', [\App\Http\Controllers\Admin\IndexController::class, 'auth']);
});

require __DIR__ . '/default.php';
